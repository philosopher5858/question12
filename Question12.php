<!DOCTYPE html>
<html lang="en">
<head>
    <style>
      .header{
    position:relative;
    display: block;
    width: 350px;
    height: 580px;
    border-radius: 10px;
    border-style: double;
    padding-top: 40px;
    padding-bottom: 50px;
    font-size: 17px;
    box-sizing: border-box;
    background:linear-gradient(rgb(63, 75, 86),rgb(86, 27, 27));
    box-shadow: 14px 14px 20px radial-gradient(rgb(63, 75, 86),rgb(86, 27, 27)) , -14px -14px 20px rgb(63, 75, 86);
    letter-spacing: 2px;
    color: rgb(255, 253, 253);
    
  }
  
  body {
    margin: 0;
    width: 100vw;
    height: 100vh;
    /* background:#ffffff; */
    background:radial-gradient(rgb(63, 75, 86),rgb(86, 27, 27));
    display: flex;
    align-items: center;
    text-align: center;
    justify-content: center;
    place-items: center;
    overflow: hidden;
    font-family: 'Courier New', Courier, monospace;
  }
  
  .container {
    position: relative;
    /* border-top-style: outset; */
    border:#121319;
    border-top-style:inset;
    border-top-color: greenyellow;
    border-bottom-style:outset ;
    border-bottom-style:groove;
    border-bottom-color:red;
    width: 500px;
    height: 100px;
    border-radius: 20px;
    padding: 80px;
    color: rgb(255, 255, 255);
    /* box-sizing: border-box; */
    background:transparent;
    /* box-shadow: 14px 14px 20px rgb(63, 75, 86), -14px -14px 20px rgb(63, 75, 86); */
    letter-spacing: 4px;
    
  }
  .inputs {
    text-align: left;
    margin-top: 3px;
  }
  
  label, input, button {
    display:inline;
    width: 100%;
    padding: 0;
    border: none;
    outline: none;
    box-sizing: border-box;
  }
  .lable2{
    margin-bottom: 40px;
    padding-right: 100%;
    font-family: 'Courier New', Courier, monospace;
  }
  
  label {
    margin-bottom: 40px;
    padding-right: 100%;
    font-family: 'Courier New', Courier, monospace;
  }
  
  label:nth-of-type(2) {
    margin-top: 12px;
    padding-right: 0%;
  }
  
  input::placeholder {
    color: gray;
  }
  
  input {
    background: #f7f9fb;
    padding: 10px;
    padding-left: 20px;
    height: 50px;
    font-size: 14px;
    border-radius: 15px;
    box-shadow: inset 0.5px 0.5px 0.5px #1abc83, inset -0.5px -0.5px 0.5px rgb(241, 139, 123);
  }
  
  button {
    color: black;
    margin-top: 20px;
    background: rgb(234, 148, 148);
    height: 40px;
    width: 220px;
    border-radius: 7px;
    cursor: pointer;
    font-weight: 900;
    /* box-shadow: 2px 2px 6px #212426, -2px -2px 6px rgb(36, 33, 33); */
    transition: .1s;
  }
  
  button:hover {
    box-shadow: none;
    background: green;
    color: white;
  }
  
  a {
    position: absolute;
    font-size: 8px;
    bottom: 4px;
    right: 4px;
    text-decoration: none;
    color: black;
    border-radius: 10px;
    padding: 2px;
  }
  
 hr{
    background-color:green;
    height: 2px;
    border-radius: 40px;
 }
 .hr1{
    background-color:red;

 }
    </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="header"> 
       Question 12: <br>	WAP to calculate the Gross Salary of an employee.
Assume Gross Salary=Basic Salary+DA+HRA
DA=40% of Basic Salary
HRA=20% of Basic Salary

    </div>
    <form action="Question12.html" method="POST">
        <hr class="hr1">
                <hr class="hr1">
        <div class="container">
        <?php
if($_SERVER['REQUEST_METHOD']=='POST'){
    $salary = $_POST['salary'];
print "Gross Salary=Basic Salary + DA + HRA <br><br>";
    $DA = ((40/100) * $salary);
    $HA = ((20/100) * $salary);
    
    $Gross_Salary = $DA + $HA + $salary;
    
    echo "<b>"."SALARY: ".$salary."<br>";
    echo " "."DA: ".$DA." ";
    echo " "."HA: ".$HA."<br>";

    echo "<b>"."<h3>"."GROSS SALARY: ".$Gross_Salary."<br>";
}

?>   
<button type="submit"> Go Back</button> 
        </div>
        <hr>
        <hr>
        </div>
        </div>
    </form>
</body>
</html>

